/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

/**
 *
 * @author joelv
 */
@Entity
@Table(name = "materiasprimas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Materiasprimas.findAll", query = "SELECT m FROM Materiasprimas m")
    , @NamedQuery(name = "Materiasprimas.findByIdmateriaPrima", query = "SELECT m FROM Materiasprimas m WHERE m.idmateriaPrima = :idmateriaPrima")
    , @NamedQuery(name = "Materiasprimas.findByCantidad", query = "SELECT m FROM Materiasprimas m WHERE m.cantidad = :cantidad")
    , @NamedQuery(name = "Materiasprimas.findByNombre", query = "SELECT m FROM Materiasprimas m WHERE m.nombre = :nombre")
    , @NamedQuery(name = "Materiasprimas.findByDescripcion", query = "SELECT m FROM Materiasprimas m WHERE m.descripcion = :descripcion")
    , @NamedQuery(name = "Materiasprimas.findByPrecio", query = "SELECT m FROM Materiasprimas m WHERE m.precio = :precio")})
public @Data class Materiasprimas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_materiaPrima")
    private Integer idmateriaPrima;
    @Column(name = "cantidad")
    private Integer cantidad;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio")
    private Integer precio;
    @JoinColumn(name = "id_proveedor", referencedColumnName = "id_proveedor")
    @ManyToOne(optional = false)
    private Proveedor idProveedor;

    public Materiasprimas() {
    }

    public Materiasprimas(Integer idmateriaPrima, Integer cantidad, String nombre, String descripcion, Integer precio, Proveedor idProveedor) {
        this.idmateriaPrima = idmateriaPrima;
        this.cantidad = cantidad;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.idProveedor = idProveedor;
    }

    

    @Override
    public String toString() {
        return null;
    }
    
}
