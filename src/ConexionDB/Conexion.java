/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConexionDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author joelv
 */
public class Conexion {

    private static Connection connection = null;

    private Conexion() {
        String url = "jdbc:mysql://localhost:3306/proyecto_erp?zeroDateTimeBehavior=convertToNull";
        String driver = "com.mysql.jdbc.Driver";
        String usuario = "root";
        String password = "";

        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, usuario, password);
            System.out.println("Conexion Exitosa");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConexion() {
        if (connection == null) {
            new Conexion();
        }
        return connection;
    }
}
